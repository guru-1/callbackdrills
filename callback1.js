function infoAboutBoard(board, boardId, callback) {
  if (arguments.length < 3 || !Array.isArray(board)) {
    throw new Error("Data insufficient");
  }
  setTimeout(() => {
    for (let index in board) {
      if (board[index].id === boardId) {
        callback(board[index]);
        return;
      }
    }
    console.log("The id entered by you did not match with our records");
  }, 2000);
}

module.exports = infoAboutBoard;
