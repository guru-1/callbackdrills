const infoAboutBoard = require("./callback1");
const listsInformation = require("./callback2");
const cardsinformation = require("./callback3");

function infoAboutBoardAndCards(board, list, cards, boardName, listName) {
  if (
    arguments.length < 5 ||
    !Array.isArray(board) ||
    typeof list !== "object" ||
    typeof cards !== "object"
  ) {
    throw new Error("Data insufficient");
  }
  setTimeout(() => {
    let boardId;
    let listId;

    for (let index in board) {
      if (board[index].name === boardName) {
        boardId = board[index].id;
        break;
      }
    }
    for (let [key, value] of Object.entries(list)) {
      for (let element in value) {
        if (value[element].name === listName) {
          listId = value[element].id;
          break;
        }
      }
    }
    infoAboutBoard(board, boardId, (boardData) => {
      console.log(boardData);
      listsInformation(list, boardId, (listData) => {
        console.log(listData);
        cardsinformation(cards, listId, (cardData) => {
          console.log(cardData);
        });
      });
    });
  }, 2000);
}

module.exports = infoAboutBoardAndCards;
