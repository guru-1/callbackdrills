const boardsAndListsOfThanosWithCardsOfMindsAndLists = require("../callback5");

const board = require(".././info/boards.json");
const list = require(".././info/lists.json");
const cards = require(".././info/cards.json");

try {
  boardsAndListsOfThanosWithCardsOfMindsAndLists(
    board,
    list,
    cards,
    "Thanos",
    "Mind",
    "Space"
  );
} catch (err) {
  console.log(err.message);
}
