const infoAboutBoard = require("../callback1.js");
const board = require(".././info/boards.json");

try {
  infoAboutBoard(board, "mcu453ed", (data) => console.log(data));
} catch (err) {
  console.log(err.message);
}

try {
  infoAboutBoard(board, "dummy", (data) => console.log(data));
} catch (err) {
  console.log(err.message);
}
