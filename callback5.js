const infoAboutBoard = require("./callback1");
const listsInformation = require("./callback2");
const cardsinformation = require("./callback3");

function boardsAndListsOfThanosWithCardsOfMindsAndLists(
  board,
  list,
  cards,
  boardName,
  listName1,
  listName2
) {
  if (
    arguments.length < 6 ||
    !Array.isArray(board) ||
    typeof list !== "object" ||
    typeof cards !== "object"
  ) {
    throw new Error("Data insufficient");
  }
  setTimeout(() => {
    let boardId;
    let listId1;
    let listId2;

    for (let index in board) {
      if (board[index].name === boardName) {
        boardId = board[index].id;
        break;
      }
    }
    for (let [key, value] of Object.entries(list)) {
      for (let element in value) {
        if (value[element].name === listName1) {
          listId1 = value[element].id;
        }
        if (value[element].name === listName2) {
          listId2 = value[element].id;
        }
      }
    }
    let listArray = [listId1, listId2];
    infoAboutBoard(board, boardId, (boardData) => {
      console.log(boardData);
      listsInformation(list, boardId, (listData) => {
        console.log(listData);
        let wait = 2000;

        for (let index in listArray) {
          setTimeout(() => {
            cardsinformation(cards, listArray[index], (cardsData) => {
              console.log(cardsData);
            });
          }, wait);
          wait += listArray[index].length * 2000;
        }
      });
    });
  }, 2000);
}

module.exports = boardsAndListsOfThanosWithCardsOfMindsAndLists;
