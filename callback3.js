function cardsinformation(cards, listId, callback) {
  if (arguments.length < 3 || typeof cards !== "object") {
    throw new Error("Data insufficient");
  }
  setTimeout(() => {
    for (let key in cards) {
      if (key === listId) {
        let wait = 2000;

        for (let obj in cards[key]) {
          setTimeout(() => {
            callback(cards[key][obj]);
          }, wait);
          wait += 2000;
        }
        return;
      }
    }
    console.log("Data entered is not available in our memory");
  }, 2000);
}

module.exports = cardsinformation;
