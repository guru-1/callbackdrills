function listsInformation(list, boardId, callback) {
  if (arguments.length < 3 || typeof list !== "object") {
    throw new Error("Data insufficient");
  }
  setTimeout(() => {
    for (let key in list) {
      if (key === boardId) {
        callback(list[key]);
        return;
      }
    }
    console.log("The data you entered is invalid");
  }, 2000);
}

module.exports = listsInformation;
