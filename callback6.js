const infoAboutBoard = require("./callback1");
const listsInformation = require("./callback2");
const cardsinformation = require("./callback3");

function boardsAndListsOfThanosWithCardsDataofAllBoards(
  board,
  list,
  cards,
  boardName,
  listArray
) {
  if (
    arguments.length < 5 ||
    !Array.isArray(board) ||
    typeof list !== "object" ||
    typeof cards !== "object" ||
    !Array.isArray(listArray)
  ) {
    throw new Error("Data insufficient");
  }
  setTimeout(() => {
    let boardId;

    for (let index in board) {
      if (board[index].name === boardName) {
        boardId = board[index].id;
      }
    }
    infoAboutBoard(board, boardId, (boardData) => {
      console.log(boardData);
      listsInformation(list, boardId, (listData) => {
        console.log(listData);
        let wait = 2000;

        for (let index in listArray) {
          setTimeout(() => {
            cardsinformation(cards, listArray[index], (cardsData) => {
              console.log(cardsData);
            });
          }, wait);
          wait += listArray[index].length * 2000;
        }
      });
    });
  }, 2000);
}

module.exports = boardsAndListsOfThanosWithCardsDataofAllBoards;
